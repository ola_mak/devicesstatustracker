package com.aleksandramak.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StatusValidator implements ConstraintValidator<Status, String> {
    Set<String> CATEGORIES = new HashSet<String>
            (Arrays.asList("NEW", "UNHEALTHY", "OK", "STALE", null));


    public boolean isValid(String value, ConstraintValidatorContext context) {
        return CATEGORIES.contains(value);
    }

}
