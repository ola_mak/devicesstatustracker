package com.aleksandramak.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StatusUpdateValidator implements ConstraintValidator<StatusUpdate, String> {
    Set<String> CATEGORIES = new HashSet<String>
            (Arrays.asList("UNHEALTHY", "OK"));


    public boolean isValid(String value, ConstraintValidatorContext context) {
        return CATEGORIES.contains(value);
    }

}
