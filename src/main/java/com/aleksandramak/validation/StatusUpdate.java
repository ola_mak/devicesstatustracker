package com.aleksandramak.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(value={FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = StatusUpdateValidator.class)
@Documented
public @interface StatusUpdate {

    String message() default "Status is not allowed.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

