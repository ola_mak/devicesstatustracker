package com.aleksandramak.repository;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
public class DeviceRepository {

    ConcurrentHashMap<Long, Device> devices = new ConcurrentHashMap<>();
    private Long counter = 0L;

    public Optional<Device> findById(Long id) {
        Assert.notNull(id, "The given id must not be null!");
        return Optional.ofNullable(devices.get(id));
    }

    public synchronized void saveDevice(Device device) {
        counter += 1;
        Date date = new Date();

        device.setId(counter);
        device.setUpdatedDate(date);

        devices.put(counter, device);
    }

    public List<Device> getAllDevices() {
        List<Device> listOfDevices = new ArrayList<>(devices.values());
        return listOfDevices;
    }

    public List<Device> getDeviceByStatus(String status) {
        List<Device> listOfDevices = devices.values().stream()
                .filter(device -> device.getStatus().equals(status))
                .collect(Collectors.toList());
        return listOfDevices;
    }

    public void updateDeviceStatus(Long id, String status) {
        Device device = devices.get(id);
        Date date = new Date();

        Device newDevice = Device.builder()
                .id(id)
                .name(device.getName())
                .secretKey(device.getSecretKey())
                .status(status)
                .updatedDate(date)
                .build();

        devices.replace(id, newDevice);
    }
}
