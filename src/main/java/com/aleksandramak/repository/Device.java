package com.aleksandramak.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device {
    private Long id;
    private String name;
    private String secretKey;
    private String status;
    private Date updatedDate;
}
