package com.aleksandramak.dto;

import lombok.Data;

@Data
public class DeviceListingDto {
    private Long id;
    private String name;
    private String status;
}
