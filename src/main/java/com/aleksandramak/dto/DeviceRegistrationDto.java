package com.aleksandramak.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DeviceRegistrationDto {
    @NotBlank(message = "Name cannot be null or blank")
    private String name;
    @NotBlank(message = "SecretKey cannot be null or blank")
    private String secretKey;
}
