package com.aleksandramak.dto;

import com.aleksandramak.validation.StatusUpdate;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DeviceStatusUpdateDto {
    @NotBlank(message = "SecretKey cannot be null or blank")
    private String secretKey;
    @StatusUpdate
    private String status;
}
