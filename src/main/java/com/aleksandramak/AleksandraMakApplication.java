package com.aleksandramak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AleksandraMakApplication {

	public static void main(String[] args) {
		SpringApplication.run(AleksandraMakApplication.class, args);
	}

}
