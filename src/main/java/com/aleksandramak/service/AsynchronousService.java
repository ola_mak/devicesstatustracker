package com.aleksandramak.service;

import com.aleksandramak.repository.Device;
import com.aleksandramak.repository.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class AsynchronousService{

    @Autowired
    private DeviceRepository deviceRepository;

    @Scheduled(fixedRate = 30000)
    public void automaticCheckAndChangeStatus() {

        List<Device> devices = deviceRepository.getDeviceByStatus("OK");
        for (Device device : devices) {
            Date d = device.getUpdatedDate();
            d.getTime();
            if (System.currentTimeMillis() - d.getTime() > TimeUnit.MINUTES.toMillis(5)) {
                deviceRepository.updateDeviceStatus(device.getId(), "STALE");
                log.info("Status of device " + device.getName() + " was changed to STALE");
            }
        }
    }
}


