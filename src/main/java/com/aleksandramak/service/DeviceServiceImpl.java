package com.aleksandramak.service;

import com.aleksandramak.converter.Converter;
import com.aleksandramak.dto.DeviceDto;
import com.aleksandramak.dto.DeviceListingDto;
import com.aleksandramak.dto.DeviceRegistrationDto;
import com.aleksandramak.dto.DeviceStatusUpdateDto;
import com.aleksandramak.repository.Device;
import com.aleksandramak.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    DeviceRepository deviceRepository;
    @Autowired
    Converter converter;

    public DeviceDto getDevice(Long id){
        return deviceRepository.findById(id).map(x->converter.convertDevicetoDeviceDto(x)).orElse(null);
//        return converter.convertDevicetoDeviceDto(deviceRepository.findById(id).orElse(null));
    }

    public void addDevice(DeviceRegistrationDto deviceRegistrationDto) {
        Device device = converter.convertDeviceDtoToDevice(deviceRegistrationDto);
        device.setStatus("NEW");
        deviceRepository.saveDevice(device);
    }

    public List<DeviceListingDto> getDevices(String status) {
        List<Device> devices;
        if (status == null) {
            devices = deviceRepository.getAllDevices();
        } else {
            devices = deviceRepository.getDeviceByStatus(status);
        }
        return converter.convertDeviceListToDeviceListingDtoList(devices);
    }

    public void updateDeviceStatus(Long id, DeviceStatusUpdateDto deviceStatusUpdateDto) {
        Optional<Device> device = deviceRepository.findById(id);
        String secretKey = device
                .map(Device::getSecretKey)
                .orElse("");
        if(deviceStatusUpdateDto.getSecretKey().equals(secretKey)) {
            deviceRepository.updateDeviceStatus(id, deviceStatusUpdateDto.getStatus());
        } else {
            throw new NoSuchElementException("Id or secretKey does not exist");
        }
    }
}
