package com.aleksandramak.service;

import com.aleksandramak.dto.DeviceDto;
import com.aleksandramak.dto.DeviceListingDto;
import com.aleksandramak.dto.DeviceRegistrationDto;
import com.aleksandramak.dto.DeviceStatusUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DeviceService {
    DeviceDto getDevice(Long id);
    void addDevice(DeviceRegistrationDto deviceRegistrationDto);
    List<DeviceListingDto> getDevices(String status);
    void updateDeviceStatus(Long id, DeviceStatusUpdateDto deviceStatusUpdateDto);
}
