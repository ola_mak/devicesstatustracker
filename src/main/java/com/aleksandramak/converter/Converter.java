package com.aleksandramak.converter;

import com.aleksandramak.dto.DeviceDto;
import com.aleksandramak.dto.DeviceListingDto;
import com.aleksandramak.dto.DeviceRegistrationDto;
import com.aleksandramak.repository.Device;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Converter {

    private static final ModelMapper objectMapper = new ModelMapper();

    public DeviceDto convertDevicetoDeviceDto(Device device) {
        DeviceDto deviceDto = objectMapper.map(device, DeviceDto.class);
        return deviceDto;
    }

    public Device convertDeviceDtoToDevice(DeviceRegistrationDto deviceRegistrationDto){
        Device device = objectMapper.map(deviceRegistrationDto, Device.class);
        return device;
    }

    public DeviceListingDto convertDeviceToDeviceListingDto(Device device) {
        DeviceListingDto deviceListingDto = objectMapper.map(device, DeviceListingDto.class);
        return deviceListingDto;
    }

    public List<DeviceListingDto> convertDeviceListToDeviceListingDtoList(List<Device> devices) {
        List<DeviceListingDto> deviceListingDtos = new ArrayList<>();
        for(Device device : devices) {
            deviceListingDtos.add(convertDeviceToDeviceListingDto(device));
        }
        return deviceListingDtos;
    }
}
