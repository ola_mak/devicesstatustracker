package com.aleksandramak.controller;

import com.aleksandramak.dto.DeviceDto;
import com.aleksandramak.dto.DeviceListingDto;
import com.aleksandramak.dto.DeviceRegistrationDto;
import com.aleksandramak.dto.DeviceStatusUpdateDto;
import com.aleksandramak.service.DeviceService;
import com.aleksandramak.validation.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping(path="/api")
public class Controller {

    @Autowired
    DeviceService deviceService;

    @RequestMapping(path="/devices/{id}"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public DeviceDto getDevice(@PathVariable Long id) {
        return deviceService.getDevice(id);
    }

    @RequestMapping(path="/devices"
        , method = RequestMethod.POST
        , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void registerDevice(@RequestBody @Valid DeviceRegistrationDto deviceRegistrationDto) {
        deviceService.addDevice(deviceRegistrationDto);
    }

    @RequestMapping(path="/devices"
        , method = RequestMethod.GET
        , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<DeviceListingDto> getDevices(@RequestParam (name = "status", required = false) @Status String status) {
        return deviceService.getDevices(status);
    }

    @RequestMapping(path = "/devices/{id}"
        , method = RequestMethod.PUT
        , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateDeviceStatus(@PathVariable Long id, @RequestBody @Valid DeviceStatusUpdateDto deviceStatusUpdateDto) {
        deviceService.updateDeviceStatus(id, deviceStatusUpdateDto);
    }
}
