package com.aleksandramak.repository;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DeviceRepositoryTest {

    DeviceRepository deviceRepository = new DeviceRepository();

    @Test
    public void testFindById() {

        Device device = Device.builder()
                .id(1L)
                .name("name")
                .secretKey("abc")
                .status("NEW")
                .updatedDate(new Date())
                .build();

        ConcurrentHashMap devices = Mockito.mock(ConcurrentHashMap.class);
        deviceRepository.devices = devices;
        when(devices.get(1L)).thenReturn(device);

        Optional<Device> returnedDevice = deviceRepository.findById(1L);

        Assert.assertEquals(returnedDevice, Optional.ofNullable(device));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByIdWhenNull() {
        deviceRepository.findById(null);
    }

    @Test
    public void testSaveDevice () {
        ConcurrentHashMap devices = Mockito.mock(ConcurrentHashMap.class);
        deviceRepository.devices = devices;

        Device device = Device.builder()
                .id(1L)
                .name("name")
                .secretKey("abc")
                .status("NEW")
                .build();

        Device deviceToSave = Device.builder()
                .id(1L)
                .name("name")
                .secretKey("abc")
                .status("NEW")
                .updatedDate(new Date())
                .build();

        deviceRepository.saveDevice(device);
        verify(devices).put(1L, deviceToSave);
    }
}
