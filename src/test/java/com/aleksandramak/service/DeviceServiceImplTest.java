package com.aleksandramak.service;

import com.aleksandramak.converter.Converter;
import com.aleksandramak.dto.DeviceDto;
import com.aleksandramak.repository.Device;
import com.aleksandramak.repository.DeviceRepository;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.verify;

public class DeviceServiceImplTest {

    DeviceServiceImpl deviceService = new DeviceServiceImpl();

    @Test
    public void testGetDevice() {
        DeviceRepository deviceRepositoryTest =  Mockito.mock(DeviceRepository.class);
        deviceService.deviceRepository = deviceRepositoryTest;
        Converter converter =  Mockito.mock(Converter.class);
        deviceService.converter = converter;

        Device device = Device.builder()
                .id(1L)
                .name("name")
                .secretKey("abc")
                .status("NEW")
                .updatedDate(new Date())
                .build();

        DeviceDto deviceDto = DeviceDto.builder()
                .name("name")
                .secretKey("abc")
                .status("NEW")
                .build();

        Optional<Device> optionalDevice = Optional.ofNullable(device);

        Mockito.when(deviceRepositoryTest.findById(1L)).thenReturn(optionalDevice);

        deviceService.getDevice(1L);

        verify(converter).convertDevicetoDeviceDto(optionalDevice.get());
    }

}
